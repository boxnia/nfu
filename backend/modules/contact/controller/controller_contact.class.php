
<?php

class controller_contact {

    public function __construct() {
        $_SESSION['modules'] = "contact";
    }

    /*public function view_contact() {
        require_once(VIEW_PATH_INC."header.html"); 
        require_once(VIEW_PATH_INC."menu.php");

        loadView(CONTACT_VIEW_PATH, 'contact3.php');

        require_once(VIEW_PATH_INC."footer.html");
    }*/

    public function check_contact() {        
        
        $jsondata = array();    
                
        
        /*if ($_POST['token'] === "contact_form") {*/
        if ($_POST['name']!=="" || $_POST['email']!=="" || $_POST['message']!=="") {//$_POST['mensaje']['multipleSelect'] ||
            
            
            //////////////// Envio del correo al usuario
            /*$arrArgument = array(
                'type' => 'contact',
                'token' => '',
                'inputName' => $_POST['inputName'],
                'inputEmail' => $_POST['inputEmail'],
                'inputSubject' => $_POST['inputSubject'],
                'inputMessage' => $_POST['inputMessage']
            );*/
            /*Por angular*/
            $arrArgument = array(
                'type' => 'contact',
                'token' => '',
                'inputName' => $_POST['name'],
                'inputEmail' => $_POST['email'],
                //'inputSubject' => $_POST['mensaje']['multipleSelect'],
                'inputSubject' => $_POST['mensaje'],
                'inputMessage' => $_POST['message']
            );
            
            set_error_handler('ErrorHandler');
            try {
                if (enviar_email($arrArgument)) {
                     
                   
                    /*$mensaje = "Su mensaje a sido enviado";
                    $jsondata["success"] = true;
                    $jsondata["mensaje"] = $mensaje;
                    echo json_encode($jsondata);       */            
                    //echo "<div class='alert alert-success'>Su mensaje a sido enviado</div>";
                } else {
                    $mensaje = "Error en el servidor, por favor intentelo mas tarde.";
                    $jsondata["success"] = false;
                    $jsondata["mensaje"] = $mensaje;
                    echo json_encode($jsondata);
                    exit;
                    //echo "<div class='alert alert-error'>Error en el servidor, por favor intentelo mas tarde.</div>";
                }
            } catch (Exception $e) {
                $mensaje = "Error en el servidor, por favor intentelo mas tarde.";
                $jsondata["success"] = false;
                $jsondata["mensaje"] = $mensaje;
                echo json_encode($jsondata);
                exit;
                //echo "<div class='alert alert-error'>Error en el servidor, por favor intentelo mas tarde.</div>";
            }
            restore_error_handler();


            //////////////// Envio del correo al admin de la ap web
            /*$arrArgument = array(
                'type' => 'admin',
                'token' => '',
                'inputName' => $_POST['inputName'],
                'inputEmail' => $_POST['inputEmail'],
                'inputSubject' => $_POST['inputSubject'],
                'inputMessage' => $_POST['inputMessage']
            );*/
            
            /*Por angular*/
            $arrArgument = array(
                'type' => 'admin',
                'token' => '',
                'inputName' => $_POST['name'],
                'inputEmail' => $_POST['email'],
                //'inputSubject' => $_POST['mensaje']['multipleSelect'],
                'inputSubject' => $_POST['mensaje'],
                'inputMessage' => $_POST['message']                
            );
            set_error_handler('ErrorHandler');
            try {
                if (enviar_email($arrArgument)) {
                    $mensaje = "Su mensaje ha sido enviado";
                    $jsondata["success"] = true;
                    $jsondata["mensaje"] = $mensaje;
                    echo json_encode($jsondata); 
                    exit;
                    /*mirr porque esta petando en la funcion enviar email, ya que los parametros pasados son los correctos*/
                    /*echo "<div class='alert alert-success'>Su mensaje a sido enviado</div>";*/
                } else {
                    $mensaje = "Error en el servidor, por favor intentelo mas tarde.";
                    $jsondata["success"] = false;
                    $jsondata["mensaje"] = $mensaje;
                    echo json_encode($jsondata);
                    exit;
                    //echo "<div class='alert alert-error'>Error en el servidor, por favor intentelo mas tarde.</div>";
                }
            } catch (Exception $e) {
                $mensaje = "Error en el servidor, por favor intentelo mas tarde.";
                $jsondata["success"] = false;
                $jsondata["mensaje"] = $mensaje;
                echo json_encode($jsondata);
                exit;
                //echo "<div class='alert alert-error'>Error en el servidor, por favor intentelo mas tarde.</div>";
            }
            
            restore_error_handler();
        } else {
            $mensaje = "Error en el servidor, por favor intentelo mas tarde o asegurse de que todos os campos han sido rellenados";
            $jsondata["success"] = false;
            $jsondata["mensaje"] = $mensaje;
            echo json_encode($jsondata);
            exit;
           //echo "<div class='alert alert-error'>Error en el servidor, por favor intentelo mas tarde.</div>";
        }
    }

}
