<?php

class controller_installation {

//controller_services será cargado tanto desde lis_services.js como por index.php

    public function __construct() {

        include(UTILS_INSTALLATION . "utils.inc.php");
        $_SESSION['users'][0];

        $_SESSION['module'] = "installation";
    }

    //////////////////////////////////list all installation
    public function list_installation() {


        set_error_handler('ErrorHandler');

        try {
//loadmodel
//$path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_6/modules/installation/model/model/';
            $results = loadModel(MODEL_INSTALLATION, "installation_model", "list_install");


//throw new Exception(); //que entre en el catch
        } catch (Exception $e) {
            $jsondata["success"] = false;
            $jsondata["type_error"] = "503";
            // showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
        }
        restore_error_handler();

        if ($results) {
             $jsondata["success"] = true;
            $jsondata["nom_install"] = $results;
            echo json_encode($jsondata);
            exit;
        } else {
//if($nom_installationos){ //que lance error si no hay installationos
            $jsondata["success"] = false;
            $jsondata["error"] = "No se ha encontrado ninguna instalación";
        }
    }

    /////////////////////////////////////////details Installation
    public function idInstallation() {

        if ($_GET["id"]) {


            $result = filter_num_int($_GET["id"]);

            if ($result['resultado']) {

                $id = $result['datos'];
            } else {
                $id = 1;
            }
            set_error_handler('ErrorHandler');
            try {
// throw new Exception(); //para probar que entre en el catch
//$path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_5/modules/installation/model/model/';
                $install = loadModel(MODEL_INSTALLATION, "installation_model", "details_install", $id);
            } catch (Exception $e) {

                $jsondata["success"] = false;
                $jsondata["type_error"] = "503";
                echo json_encode($jsondata);
                exit;
            }
            restore_error_handler();

            if ($install) {
                $jsondata["success"] = true;
//require_once 'modules/services/view/details_services.php';
//loadView('modules/installation/view/', 'details_installations.php', $install[0]);
                $jsondata["install"] = $install[0];
                echo json_encode($jsondata);
                exit;
            } else {

                $jsondata["success"] = false;
                $jsondata["type_error"] = "404";
                echo json_encode($jsondata);
                exit;
            }
        }
    }

}
