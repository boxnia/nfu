<?php
    $path = $_SERVER['DOCUMENT_ROOT'] . '/NFU/';
    define('SITE_ROOT', $path);
    require(SITE_ROOT . "modules/games/model/DAO/game_dao.class.singleton.php");

    class game_bll {
        private $dao;
        private $db;
        static $_instance;
    
        private function __construct() {
            $this->dao = game_dao::getInstance();
            $this->db = db::getInstance();
        }
    
        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        public function create_game_BLL($arrArgument) {
            
            return $this->dao->create_game_DAO($this->db, $arrArgument);
            
        }
        
        public function create_ins_BLL($arrArgument) {
            
            return $this->dao->create_ins_DAO($this->db, $arrArgument);
            
        }
        
        
        
        /*public function obtain_countries_BLL($url) {
            return $this->dao->obtain_countries_DAO($url);
        }*/
        
        public function obtain_provinces_BLL() {
            return $this->dao->obtain_provinces_DAO();
        }
        
        public function obtain_populations_BLL($arrArgument) {
            return $this->dao->obtain_populations_DAO($arrArgument);
        }
        
        public function filter_install_nombre_BLL($criteria) {
            return $this->dao->filter_install_nombre_DAO($this->db, $criteria);
        }
        
        public function get_coor_install_BLL($criteria) {
            return $this->dao->get_coor_install_DAO($this->db, $criteria);
        }
        
        

}
