<?php
function validate_game($value) {
/*esto iva dentro del array flitro'pricecash' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[0-9]+(\.[0-9]+)?$/')
),*/
/*esto va dentro del if resultado, if (!$resultado['pricecash']) {
        $error['pricecash'] = 'El precio de la partida no es válido o esta vacio';
        $resultado['pricecash'] = $value['pricecash'];
        $valido = false;
}*/
    $error = array();
    $valido = true;
    $filtro = array(
        'name' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^\D{3,30}$/')
        ),
        'time' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9] (AM|PM)$/')
        ),
        'duration' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0|3][0]:[0][0]$/')
        ),        
        'places' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^([0-9]+)$/')
        ),
        'day' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/\d{2}.\d{2}.\d{4}$/')
        )        
    );

    $resultado = filter_var_array($value, $filtro);



    
    
    if($value['pricecash'] === "Gratuito"){
        $resultado['pricecash'] = $value['pricecash'];
        $valido = true;
    }else{
        if (preg_match('/^[A-Za-z]{1}[A-Za-z0-9]{5,31}$/', "moo123")) {
            $resultado['pricecash'] = $value['pricecash'];
            $valido = true;
        } else {
            $resultado['pricecash'] = $value['pricecash'];
            $error['error'] = 'La cantidad economica no es correcta';
            $valido = false;
        }
 
    }
    
    if ($value['day'] === "") {
        $error['error'] = 'El campo esta vacio';
        $resultado['day'] = $value['day'];
        $valido = false;
    } else {
        if (!valida_dates_games($value['day'])) {
            $error['error'] = 'la fecha de la partida es anterior a la actual';
            $resultado['day'] = $value['day'];
            $valido = false;
        }
    }
    
    $resultado['sport'] = $value['sport'];
    $resultado['provincia'] = $value['provincia'];    
    $resultado['poblacion'] = $value['poblacion'];
    
    
    

    if ($resultado != null && $resultado) {


        if (!$resultado['name']) {
            $error['error'] = 'El nombre de la partida introducido no es válido';
            $resultado['name'] = $value['name'];
            $valido = false;
        }

        if (!$resultado['time']) {
            $error['error'] = 'La hora de inicio introducida no es válida    ';
            $resultado['time'] = $value['time'];
            $valido = false;
        }

        if (!$resultado['duration']) {
            $error['error'] = 'La duración de la partida introducida no es válida';
            $resultado['duration'] = $value['duration'];
            $valido = false;
        }        

        if (!$resultado['places']) {
            $error['error'] = 'EL campo introducido no es válido o el campo esta vacio';
            $resultado['places'] = $value['places'];
            $valido = false;
        }

        if (!$resultado['day']) {
            $error['error'] = 'La fecha introducida no es válida';
            $resultado['day'] = $value['day'];
            $valido = false;
        }
        
    } else {
        $valido = false;
    }

    return $return = array('resultado' => $valido, 'error' => $error, 'datos' => $resultado);
}

function valida_dates_games($start_day) {
    $time = time();
    $nowdate = date("Y/m/d ", $time);
    list($mes_one, $día_one, $anio_one) = split('/', $start_day);    
    list($anio_two,$mes_two, $día_two) = split('/', $nowdate);
    
    
    

    $dateOne = new DateTime($anio_one . "/" . $mes_one . "/" . $día_one);
    $dateTwo = new DateTime($anio_two . "/" . $mes_two . "/" . $día_two);
    if ($dateOne >= $dateTwo) {
        return true;
    }
    return false;
}

function coordenadas($provincia,$poblacion){

    $direccion_google = $provincia.", ".$poblacion;
    /*nom hemos kedado mirando la funcionalidad de esta funcion*/
    /*$direccion_google = $direccion;*/
    $resultado = file_get_contents(sprintf('https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=%s', urlencode($direccion_google)));
    $resultado = json_decode($resultado, TRUE);

    $lat = $resultado['results'][0]['geometry']['location']['lat'];
    $lng = $resultado['results'][0]['geometry']['location']['lng'];

    $coor = Array($lat, $lng);     
    return $coor;
    }

    
function search_coor($usersJSON) {
    if ($usersJSON["poblacion"] && $usersJSON["contact_install"]) {
        //$criteria = json_decode($_POST["data_games_pob_JSON"], true);
        
        $criteria = array(                    
                    'provincia' => $usersJSON["provincia"],
                    'poblacion' => $usersJSON["poblacion"],
                    'instalacion' => $usersJSON["contact_install"]
                );
        
        set_error_handler('ErrorHandler');

        try {
//loadmodel
//$path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_6/modules/installation/model/model/';
            $results = loadModel(MODEL_GAME, "game_model", "get_coor_install", $criteria); //$criteria['poblacion']
//throw new Exception(); //que entre en el catch
        } catch (Exception $e) {
            //$jsondata["success"] = false;
            //$jsondata["mensaje"] = "Hay un problema en la consulta a base de datos, porfavor intentelo mas tarde";
            //echo json_encode($jsondata);
            //exit;
            $results = "";
            return $results;
            /* showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503); */
        }
        restore_error_handler();

        if ($results) {
            return $results;
            //$jsondata["success"] = true;
            //$jsondata["ubucacion_install"] = $results;
            //echo json_encode($jsondata);
            //exit;
        } else {
            $results = "";
            return $results;
//if($nom_installationos){ //que lance error si no hay installationos
            //$jsondata["success"] = false;
            //$jsondata["mensaje"] = "No se ha encontrado ninguna intalacion en esta poblacion";
            //echo json_encode($jsondata);
            //exit;
        }
    }
}

/*function validar_facilities_checked($facilities) {
    $error = "";

    if (empty($facilities)) {
        $error = "No ha elegido ninguna opcion, elija minimo 2";
    } else {
        $no_checked = count($value['color']);
        if ($no_checked < 2)
            $error = "Elija almenos 2 opciones";
    }
    return $error;
}*/

/*function validate_poblacion($poblacion) {
    //$poblacion = addslashes($poblacion);
    $poblacion = filter_var($poblacion, FILTER_SANITIZE_STRING);
    return $poblacion;
}*/