<?php

function validate_users_update($value) {


    $error = array();
    $valido = true;
    $error1 = "";

    $error2 = "";
    $filtro = array(
        /* 'codigo' => array(
          'filter' => FILTER_VALIDATE_REGEXP,
          'options' => array('regexp' => '/^.{5,10}$/')
          ), */
        
        'email' => array(
            'filter' => FILTER_CALLBACK,
            'options' => 'validatemail'
        ),
        'password' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^.{6,12}$/')
        ),
        'password2' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^.{6,12}$/')
        ),
        /*  'password_old' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^.{6,12}$/')
        ),*/
        'fecha_nac' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/\d{2}.\d{2}.\d{4}$/')
        ),
        'telefono' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^(6|7)[0-9]{8}$/')
        ),/*
              'fecha_registro' => array(
              'filter' => FILTER_VALIDATE_REGEXP,
              'options' => array('regexp' => '/\d{2}.\d{2}.\d{4}$/')
              ), */
    );

    $resultado = filter_var_array($value, $filtro);
    
    //$resultado['deportes'] = $value['deportes'];
    /*  if (!$resultado['nivel']) {
      //	$error['nivel'] = 'Seleccionar un deporte';
      } else {
      $resultado['nivel'] = $value['nivel'];
      $valido = true;
      }
     */
    /* $error1 = validar_facilities_checked($value['deportes']);

      if ($error1 !== "") {
      $error['deportes'] = $error1;

      $valido = false;
      } else {
      $resultado['deportes'] = $value['deportes'];
      };
     */



    if ($resultado != null && $resultado) {
        if($value['password']&& $value['password2']){
        $error2 = validar_passwd($value['password'], $value['password2']);

        if ($error2 !== "") {
            $error['password'] = $error2;

            $valido = false;
        } else {
            $resultado['password'] = $value['password'];
            $valido = true;
        }
        }else{
             $resultado['password'] = $value['password'];
             $resultado['password_old'] = $value['password_old'];
            //$valido = true;
            $valido = true;
        }

        /* if (!$resultado['codigo']) {
          $error['codigo'] = 'El codigo introducido no es valido, mínimo 5 carácteres';
          $resultado['codigo'] = $value['codigo'];
          $valido = false;
          }
         */
     
           // $resultado['password_old'] = $value['password_old'];
            //$valido = true;
        
        if (!$resultado['email']) {
            $error['email'] = 'El email no es valido';
            $resultado['email'] = $value['email'];
            $valido = false;
        } else {
            $resultado['email'] = $value['email'];
            $valido = true;
        }
        
         if ($resultado['telefono']) {
             $resultado['telefono'] = $value['telefono'];
            $valido = true;
        } else {
          
        }
        /* 	if(!$resultado['password'] || $resultado['password']!=$_POST['password2'] ){
          $error['password'] = 'Password debe tener de 6 a 12 caracteres y las dos contrasenyas deben ser iguales';
          $resultado['password'] = $_POST['password'];
          $valido = false;
          }

         */
        
       
        if ($value['fecha_nac'] === "" || $value['fecha_registro'] === "" || !compruebaFecha($value['fecha_nac']) || !valida_dates($value['fecha_nac'], $value['fecha_registro'])) {


            $error['fecha_nac'] = 'El usuario debe ser mayor de edad';
            $error['fecha_registro'] = 'El campo esta vacio';
            $resultado['fecha_nac'] = $value['fecha_nac'];
            $valido = false;
        } else if (!compruebaFecha($value['fecha_nac'])) {

            $error['fecha_nac'] = 'La fecha no es válida';
            $valido = false;
        } else {
            $resultado['fecha_nac'] = $value['fecha_nac'];

            $resultado['fecha_registro'] = $value['fecha_registro'];
            $valido = true;
        }
    } else {
        $valido = false;
    }
    $return = array('resultado' => $valido, 'error' => $error, 'datos' => $resultado);

    return $return;
}

function valida_dates($start_day, $daylight) {
 
    // list($mes_one, $dia_one, $anio_one) = split('/', $start_day);
    //list($mes_two, $dia_two, $anio_two)= split('/', $daylight);


    $date = date_create_from_format("m-d-Y", $start_day);


    $myDateTime = date_format($date, "Y/m/d");


    $date1 = date_create_from_format("Y-m-d", $daylight);


    $myDateTime1 = date_format($date1, "Y/m/d");





    $newDate = strtotime($myDateTime);
    $newDate1 = strtotime($myDateTime1);



    $days_between = ceil(abs($newDate - $newDate1) / 84600);

    //pongo 365 para no buscar la fecha, para ser mayor de 18 años, tendría que ser mayor de 6570 días.
    if ($days_between > 6570) {
        return true;
    }
    return false;


}


function validatemail($email) {
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        if (filter_var($email, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^.{5,50}$/')))) {
            return $email;
        }
    }
    return false;
}



function validar_passwd($passwd, $passwd2) {
    $errorpassw = "";
    if (strlen($passwd) < 6) {
        return $errorpassw = "Password debe tener al menos 6 caracteres";
    }
    if (strlen($passwd) > 16) {
        return $errorpassw = "Password no puede tener m�s de 16 caracteres";
    }
    if (!preg_match('`[a-z]`', $passwd)) {
        return $errorpassw = "Password debe tener al menos una letra minúscula";
    }
    if (!preg_match('`[A-Z]`', $passwd)) {
        return $errorpassw = "Password debe tener al menos una letra mayúscula";
    }
    if (!preg_match('`[0-9]`', $passwd)) {
        return $errorpassw = "Password debe tener al menos un caracter numérico";
    }

    if ($passwd != $passwd2) {
        return $errorpassw = "Los password deben coincidir";
    }
    return $errorpassw = "";
}

function compruebaFecha($date) {


    $test_arr = explode('-', $date);
    if (count($test_arr) == 3) {
        if (checkdate($test_arr[0], $test_arr[1], $test_arr[2])) {
            // valid date ...
            return true;
        } else {
            // problem with dates ...
            return false;
        }
    } else {
        return false;
        // problem with input ...
    }
}

function get_user($token){
    set_error_handler('ErrorHandler');
            try {

                $arrValue = loadModel(MODEL_USERS, "users_model", "obtain_user", $token);
                
            } catch (Exception $e) {
                $arrValue = false;
            }
            restore_error_handler();

            if ($arrValue) {
               
                $_SESSION['users'] = $arrValue;
                
         
            } 
}


