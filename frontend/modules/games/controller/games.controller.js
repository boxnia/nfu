app.controller('GamesCtrl', function ( $scope, services, $location, $timeout, FlashService, $cookies/*, $http */) {
  //console.log("Estoy en games");
  
    
  /*////////////////////////////////////////////scope partida and atributes*/
    $scope.partida={
        gamename:"",
        sport:"futbol",
        provincia: "",
        poblacion:"",
        contact_install:"",
        zona:"",
        days_game:"",
        times_start:"08:00 AM",
        durations:"01:00:00",
        places_number:"",
        enter:"gratuito",
        price:""
        
    }
      
    /*////////////////////////////////////////////scope error*/
    $scope.error = function() {
        
        $scope.partida.e_gamename = ""; 
        $scope.partida.e_population = "";  
        $scope.partida.e_province = ""; 
        $scope.partida.e_install = "";          
        $scope.partida.e_zona = "";
        $scope.partida.e_days_game = "";
        $scope.partida.e_times_start = "";
        $scope.partida.e_durations = "";
        $scope.partida.e_places_number = "";        
        $scope.partida.e_price = "";
        $scope.partida.e_send = "";
        
    }
    
    // Store
    $scope.setvalues = function (){
      localStorage.setItem("name", $scope.partida.gamename);
      localStorage.setItem("time", $scope.partida.times_start);
      localStorage.setItem("duration", $scope.partida.durations);
      localStorage.setItem("places", $scope.partida.places_number);
      localStorage.setItem("day", $scope.partida.days_game);
      localStorage.setItem("zona", $scope.partida.zona);
    }

    // Retrieve
    $scope.getvalues = function(){

    if(localStorage.getItem("name")){
      $scope.partida.gamename = localStorage.getItem("name");
    }
    if(localStorage.getItem("time")){
      $scope.partida.times_start = localStorage.getItem("time");
    }
    if(localStorage.getItem("duration")){
      $scope.partida.durations = localStorage.getItem("duration");
    }
    if(localStorage.getItem("places")){
      $scope.partida.places_number = localStorage.getItem("places");
    }
    if(localStorage.getItem("day")){
      $scope.partida.days_game = localStorage.getItem("day");
    }
    if(localStorage.getItem("zona")){
      $scope.partida.zona = localStorage.getItem("zona");
    }

    }

    $scope.getvalues();
        
    /*////////////////////////////////////////////select the cookie for select name user type*/
    var user_cookie = Base64.decode($cookies.getObject('user'));
    var user = JSON.parse(user_cookie);
        
    if (user) {
        FlashService.load_header(user);
        FlashService.load_menu(user);
    } else {
        FlashService.load_header("");
        FlashService.load_menu("");
    }
   
    /*////////////////////////////////////////////function create game*/
    $scope.create_game = function(){
        /*////////////////////////////////////////////declarate variable*/
        var name = $scope.partida.gamename;
        var time = $scope.partida.times_start;
        var duration = $scope.partida.durations;                
        var price1 = $scope.partida.enter;
        if (price1 == "pago") {
        var pricecash = $scope.partida.price; /*solo si esta seleccionado el precio*/
        } else {
            var pricecash = "Gratuio";
        }
        var places = $scope.partida.places_number;
        var day = $scope.partida.days_game;
        var sport = $scope.partida.sport;
        var zona = $scope.partida.zona;
        
        
        /*Regular expressions*/
        var name_game = /^\D{3,30}$/;
        var time_game = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9] (AM|PM)$/;
        var duration_game = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0|3][0]:[0][0]$/;
        var price_game = /^[0-9]+(\.[0-9]+)?$/;
        var places_game = /^([0-9]+)$/;  
        var day_game = /\d{2}.\d{2}.\d{4}$/;
        /* fin Regular expressions*/
        /*////////////////////////////////////////////end declarate variable*/
        //$(".error").remove();
        /*////////////////////////////////////////////error control*/
    if ($scope.partida.gamename == "" || !name_game.test($scope.partida.gamename)) {            
            $scope.partida.e_gamename = "Introduzca un nombre de partida";
            $timeout(function () {
                $scope.partida.e_gamename = "";
            }, 5000);
        //$("#gamename").focus().after("<span class='error'>Introduzca un nombre de partida</span>");
        result = false;
        return;

    }
    if ($scope.partida.times_start == "" || !time_game.test($scope.partida.times_start)) {
            $scope.partida.e_times_start = "Introduzca la hora de inicio";
            $timeout(function () {
                $scope.partida.e_times_start = "";
            }, 5000);
        //$("#time_start").focus().after("<span class='error'>Introduzca la hora de inicio</span>");
        result = false;
        return;


    }
    if ($scope.partida.durations == "" || !duration_game.test($scope.partida.durations)) {
            $scope.partida.e_durations = "Introduzca la duración de la partida";
            $timeout(function () {
                $scope.partida.e_durations = "";
            }, 5000);
        //$("#duration").focus().after("<span class='error'>Introduzca la duración de la partida</span>");
        result = false;
        return;


    }
    if (pricecash != "Gratuio"){
        if (pricecash == "" || !price_game.test(pricecash)) {
            $scope.partida.e_price = "Introduzca un precio de la entrada";
            $timeout(function () {
                $scope.partida.e_price = "";
            }, 5000);
            //$("#price_cash").focus().after("<span class='error'>Introduzca un precio de la entrada</span>");
            result = false;
            return;
        }
    
    }
       
    if ($scope.partida.places_number == "" || !places_game.test($scope.partida.places_number)) {
            $scope.partida.e_places_number = "Introduzca las plazas disponibles"
            $timeout(function () {
                $scope.partida.e_places_number = "";
            }, 5000);
        //$("#places_number").focus().after("<span class='error'>Introduzca las plazas disponibles</span>");
        result = false;
        return;
    }
    
    if ($scope.partida.days_game == "" || !day_game.test($scope.partida.days_game)) {
            $scope.partida.e_days_game = "Introduzca la fecha de la partida";
            $timeout(function () {
                $scope.partida.e_days_game = "";
            }, 5000);
        //$("#day_game").focus().after("<span class='error'>Introduzca la fecha de la partida</span>");
        result = false;
        return;


    }
    
    /*////////////////////////////////////////////end error control*/
    
    
        /*////////////////////////////////////////////get values of population value install and user name*/
        var poblacion = $scope.partida.poblacion.poblacion;
        
        var contact_install = document.getElementById("contact_install").value;
        
        var usuario = user.nombre;       
        
        /*////////////////////////////////////////////call empoint*/
        if (result) {

            if (sport === 'tenis') {
                sport = ruta_image("Tenis.svg");
            } else if (sport === 'futbol') {

                sport = ruta_image("Futbol.svg");
            }
            else if (sport === 'baloncesto') {

                sport = ruta_image("Basket.svg");
            }
            else if (sport === 'padel') {

                sport = ruta_image("Padel.svg");
            } else if (sport === 'voleibol') {
                sport = ruta_image("Volei.svg");
            }
            
            
            var data = {"name": name, "time": time, "duration": duration, "pricecash": pricecash, "places": places, "day": day, "sport": sport,
                "poblacion": poblacion, "contact_install":contact_install, "zona":zona, "usuario":usuario};
            
            var data_games_JSON = JSON.stringify(data);
            //console.log(data_games_JSON);

            services.post('games', 'up_games', data_games_JSON).then(function (data) {
                console.log(data);
                if (data.data.success) {
                    /*//////////////////fail*/
                    //console.log(data);
                    //console.log(data.data.mensaje);
                    swal(data.data.mensaje);
                    //console.log("estamos dentro de la funcion de recovery en angular linea 111");
                    $timeout(function () {
                        $location.path('/users_games');
                    }, 3000);
                    /*$("#sed_email_recovery").focus().after("<div class='error'>" + data.mensaje + "</div>");*/

                } else {
                    /*//////////////////ok*/
                    //console.log("estamos dentro de la funcion de recovery en angular linea 118")
                    swal(data.data.mensaje);
                    //$scope.partida.e_send = data.data.mensaje;
                    $timeout(function () {
                        $scope.partida.e_send = "";
                    }, 5000);
                    //console.log("estamos dentro de la funcion de recovery en angular linea 125")

                }

            });

        }
        
        
        
        
        
        
        /*/////////////estamos creando el validate*/
    }
    
    /*////////////////////////////////////////////end call empoint*/
    /*$scope.create_game = function(){
        
    }*/
    
    
    /*////////////////////////////////////////////charge provinces*/
    $scope.load_provincias_v1 = function(){ 
        
    services.post('games', 'load_provinces', {"load_provincias": true})
            .then(function (response) {

                if (response.data === 'error') {
                    $scope.partida.e_province = "Error al recuperar la informacion de provincias";
                    $timeout(function () {
                        $scope.partida.e_province = "";
                    }, 3000);

                } else {
                    $scope.provincia = response.data.provincias;

                }
            });
    
    }
    
    /*////////////////////////////////////////////end charge provinces*/
    
    /*////////////////////////////////////////////charge populations*/
    $scope.load_poblaciones_v1 = function () {

        var datos = {idPoblac: $scope.partida.provincia.id};
        //console.log(datos);
        services.post('games', 'load_populations', datos)
                .then(function (response) {
                    if (response.data === 'error') {
                        $scope.partida.e_population = "Error al recuperar la informacion de poblaciones";
                        $timeout(function () {
                            $scope.partida.e_population = "";
                        }, 3000);

                    } else {
                        $scope.poblacion = response.data.poblaciones;
                        console.log($scope.poblacion);

                    }
                })
    }
    
    /*////////////////////////////////////////////end charge provinces*/                        
    
    
    /*////////////////////////////////////////////filter for select install*/   
    $scope.filter = function(){
        var poblacion = $scope.partida.poblacion.poblacion;
        //console.log(poblacion);
        var data_pob = {"poblacion":poblacion};
        
        
        services.post('games', 'autocomplete_installation_population', data_pob).then(function (response){
            //console.log(response);
            var ubucacion_install = response.data.ubucacion_install;
            //console.log(ubucacion_install);
            if (!response.data.success) {
                
                $scope.partida.e_install = response.data.mensaje;
                $timeout(function () {
                    $scope.partida.e_install = "";
                }, 5000);
                //$("#e_contact_install").focus().after("<center><div class='error'>" + json.mensaje + "</div></center>");
                //$('#contact_install').val('');
                $scope.partida.contact_install.valueOf("");

                
            } else {
                //console.log(response.data.data);
                var suggestions = new Array();
                for (var i = 0; i < ubucacion_install.length; i++) {
                    //suggestions.push(ubucacion_install[i].nombre);
                    suggestions.push(ubucacion_install[i]);
                }
                
                $scope.suggestions = suggestions;
                console.log($scope.suggestions);
                //$scope.name_install = suggestions;
                //console.log(suggestions);
                /*$("#contact_install").autocomplete({                
                    source: suggestions,
                    minLength: 1,                    
                });*/
            }
        });
        
    }
    
    /*////////////////////////////////////////////end filter for select install*/
    
    
    //$scope.filter();
    
    /*////////////////////////////////////////////call function*/
    $scope.load_provincias_v1();        

   
});

/*estamos creando el controlador de crear partidas.*/
app.controller('create_install', function ($location, $q, FlashService, $cookies, $scope, services, $timeout) {

    console.log("Estoy en UsersGamesCtrl");
    ///////////////////////////////////////$cookies
    var user_cookie = Base64.decode($cookies.getObject('user'));
    var user = JSON.parse(user_cookie);
    ///////////////////////////////acces page
    if (!$cookies.getObject('user')) {
        $location.path('/')
    }
    ////////////////////////////load menu and header
    if (user) {
        FlashService.load_header(user);
        FlashService.load_menu(user);
    } else {
        FlashService.load_header("");
        FlashService.load_menu("");
    }
    ///////////////////////////geolocation
    $scope.lat = "0";
    $scope.lng = "0";
    $scope.accuracy = "0";
    $scope.error = "";
    $scope.model = {myMap: undefined};
    $scope.myMarkers = [];


    $scope.create_ins={
        nombre:"",
        provincia: "",
        poblacion:"",
        latiud:"38.82238",
        longitud:"-0.60882",
        avatar_install:"",
        valoracion:"",
        descripcion:"",
        marker:""
    }

    $scope.error = function() {

        $scope.create_ins.e_name_installation = "";
        $scope.create_ins.e_provincia = "";
        $scope.create_ins.e_poblacion = "";
        $scope.create_ins.e_descripcion = "";


    }

    $scope.create_install = function(){
        
        var name = $scope.create_ins.nombre;
        var longitud = $scope.create_ins.longitud;
        var latitud = $scope.create_ins.latiud;
        var descripcion = $scope.create_ins.descripcion;
        var poblacion = $scope.create_ins.poblacion.poblacion;
        var valoracion = "Nivel.svg";
        var name_ins = /^\D{3,30}$/;
        var result = true;
        
        if (name == "" || !name_ins.test(name)) {
            $scope.create_ins.e_name_installation = "Introduzca un nombre de la instalación";
            $timeout(function () {
                $scope.partida.e_name_installation = "";
            }, 5000);
            //$("#gamename").focus().after("<span class='error'>Introduzca un nombre de partida</span>");
            result = false;
            return;

        }
        
        if (descripcion == "") {
            $scope.create_ins.e_descripcion = "Introduzca una breve descripcion de la instalación";
            $timeout(function () {
                $scope.create_ins.e_descripcion = "";
            }, 5000);
            //$("#gamename").focus().after("<span class='error'>Introduzca un nombre de partida</span>");
            result = false;
            return;

        }
    
    if(result){
        
        console.log(provincia);
        
        var data = {"name": name, "poblacion":poblacion, "longitud":longitud, "latitud":latitud, "descripcion":descripcion, "valoracion":valoracion};
            
        var data_games_JSON = JSON.stringify(data);
            //console.log(data_games_JSON);
            
            services.post('games', 'up_install', data_games_JSON).then(function (data) {
                console.log(data);
                if (data.data.success) {
                    /*//////////////////fail*/
                    //console.log(data);
                    //console.log(data.data.mensaje);
                    swal(data.data.mensaje);
                    //console.log("estamos dentro de la funcion de recovery en angular linea 111");
                    $timeout(function () {
                        $location.path('/games_form');
                    }, 3000);
                    /*$("#sed_email_recovery").focus().after("<div class='error'>" + data.mensaje + "</div>");*/

                } else {
                    /*//////////////////ok*/
                    //console.log("estamos dentro de la funcion de recovery en angular linea 118")
                    swal(data.data.mensaje);
                    //$scope.partida.e_send = data.data.mensaje;
                    /*$timeout(function () {
                        $scope.partida.e_send = "";
                    }, 5000);*/
                    //console.log("estamos dentro de la funcion de recovery en angular linea 125")

                }

            });
        
    }
    
    
    
    
    
    
    }
    
    
    
    




//https://developers.google.com/maps/documentation/javascript/examples/geocoding-place-id
// In the following example, markers appear when the user clicks on the map.
// Each marker is labeled with a single alphabetical character.
var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
var labelIndex = 0;
var markers = [];

function initialize() {
//$scope.initialize = function() {
  swal('¡Fijate bieen! Se lo más exacto posible, nuestros usuarios lo valoran muchísimo. Puntua sobre el mapa la instalacion que va a generar. ¡GRACIAS :)!');
  //var bangalore = { lat: 12.97, lng: 77.59 };
  var bangalore = { lat: 38.82238, lng: -0.60882 };
  //var bangalore = { $scope.create_ins.latiud, $scope.create_ins.longitud };
  var map = new google.maps.Map(document.getElementById('map_canvas'), {
    zoom: 12,
    center: bangalore
  });
  $scope.create_ins.marker = map;
    
  // This event listener calls addMarker() when the map is clicked.
  google.maps.event.addListener(map, 'click', function(event) {
    $scope.create_ins.latiud = event.latLng.lat();
   $scope.create_ins.longitud = event.latLng.lng();
    console.log($scope.create_ins.latiud + ', ' + $scope.create_ins.longitud);
    addMarker(event.latLng, map);
  });

  // Add a marker at the center of the map.
  //addMarker(bangalore, map);
  addMarker(map);
}

// Adds a marker to the map.
function addMarker(location, map) {
//$scope.addMarker = function(location, map){
  // Add the marker at the clicked location, and add the next-available label
  // from the array of alphabetical characters.
  console.log(markers.length);
  if(markers.length == 0 || markers.length == 1){
    var marker = new google.maps.Marker({
      position: location,
      label: labels[labelIndex++ % labels.length],
      map: map
    });
    markers.push(marker);

  }else{
    alert('no puede añadir varias posiciones');
  }

}

// Sets the map on all markers in the array.
function setMapOnAll(map) {
//$scope.setMapOnAll = function(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
//$scope.clearMarkers = function() {
  setMapOnAll(null);
}

// Deletes all markers in the array by removing references to them.
//function deleteMarkers() {
$scope.deleteMarkers = function() {
  clearMarkers();
  markers = [];
}

//function geocodeAddress(geocoder, resultsMap) {  
$scope.geocodeAddress = function (geocoder, resultsMap) {  
  var geocoder = new google.maps.Geocoder();
  var address = $scope.create_ins.poblacion.poblacion+", "+$scope.create_ins.provincia.nombre;
  console.log(address);
  geocoder.geocode({'address': address}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      $scope.create_ins.marker.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
        map: resultsMap,
        position: results[0].geometry.location
      });
    } else {
      alert('La Geolocalzación no se ha podido realizar, a devuelto estado: ' + status);
    }
  });
}

google.maps.event.addDomListener(window, 'load', initialize);






    /*////////////////////////////////////////////charge provinces*/
    $scope.provincias = function(){

    services.post('games', 'load_provinces', {"load_provincias": true})
            .then(function (response) {

                if (response.data === 'error') {
                    $scope.create_ins.e_provincia = "Error al recuperar la informacion de provincias";
                    $timeout(function () {
                        $scope.create_ins.e_provincia = "";
                    }, 3000);

                } else {
                    $scope.provincia = response.data.provincias;

                }
            });

    }

    /*////////////////////////////////////////////end charge provinces*/

    /*////////////////////////////////////////////charge populations*/
    $scope.poblaciones = function () {

        var datos = {idPoblac: $scope.create_ins.provincia.id};       
        //console.log(datos);
        services.post('games', 'load_populations', datos)
                .then(function (response) {
                    if (response.data === 'error') {
                        $scope.create_ins.e_poblacion = "Error al recuperar la informacion de poblaciones";
                        $timeout(function () {
                            $scope.create_ins.e_poblacion = "";
                        }, 3000);

                    } else {
                        $scope.poblacion = response.data.poblaciones;


                    }
                })
    }

    /*////////////////////////////////////////////end charge provinces*/

    $scope.provincias();


});