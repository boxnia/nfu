app.controller('LoginCtrl', function (FlashService, $scope, $location, services, $filter, $timeout, $cookies, authService, FacebookF, twitterService) {//, twitterService

    if ($cookies.getObject('user')) {
        $location.path('/users')
    }
    console.log("estoy en login")
    $scope.myRegex_name = /^([a-z ñáéíóú]{2,60})$/i;
    $scope.myRegex_password = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    $scope.myRegex_email = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    $scope.showLoader = true;
    if ($cookies.getObject('user')) {

        ////////////////////////////////////$cookie
        var user_parse = Base64.decode($cookies.getObject('user'));
        //console.log(user);
        var user = JSON.parse(user_parse);
    }

    ////////////////////////////////////load menu and header
    if (user) {
        FlashService.load_header(user);
        FlashService.load_menu(user);
    } else {
        FlashService.load_header("");
        FlashService.load_menu("");
    }





    ////////////////valores form login
    $scope.login = {
        nombre: "",
        password: "",
        password2: "",
        email: "",
        fecha_nac: "",
        fecha_registro: $filter('date')(new Date(), 'yyyy-MM-dd'),
        fecha: "",
    }

    /////////////valores form  signin
    $scope.signin = {
        nombre: "",
        password: "",
    }
    ////////////////////////////empty mistakes login
    $scope.change = function () {
        $scope.login.nombre_error = "";
        $scope.login.email_error = "";
    }
///////////////////////////////geolocation

    $scope.showPosition = function (position) {


        $scope.lat = position.coords.latitude;
        $scope.lng = position.coords.longitude;



        $cookies.put('latitud', $scope.lat);
        $cookies.put('longitud', $scope.lng);



    }
    $scope.showError = function (error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                $scope.error = "User denied the request for Geolocation."
                break;
            case error.POSITION_UNAVAILABLE:
                $scope.error = "Location information is unavailable."
                break;
            case error.TIMEOUT:
                $scope.error = "The request to get user location timed out."
                break;
            case error.UNKNOWN_ERROR:
                $scope.error = "An unknown error occurred."
                break;
        }
        $scope.$apply();
    }
    $scope.getLocation = function () {

        if (navigator.geolocation) {


            navigator.geolocation.getCurrentPosition($scope.showPosition, $scope.showError)



        }
        else {
            $scope.error = "Geolocation is not supported by this browser.";

        }

    }
    $scope.getLocation();
///////////////////////////////////////////////facebook
    var userIsConnected = false;
    $scope.IntentLogin = function () {
        if (!userIsConnected) {
            FacebookF.login().then(function () {
                FacebookF.me().then(function (response) {
                    // var response_js=JSON.stringify(response); 
                    services.post('login', 'social_facebook', response)
                            .then(function (data) {
                                console.log(data)
                                if (data.data.success) {
                                    console.log(data.data.users)

                                    console.log(data.data.users);
                                    authService.SetCredentials(data.data.users);

                                    $cookies.put('avatar', data.data.avatar);
                                    console.log($cookies.getObject('user'));
                                    window.location = data.data.redirect;
                                } else {

                                    swal(response.error);

                                }

                            });

                })
            })
        }
    };

    ////////////////////////////alta user
    $scope.altaUser = function () {

        var user = JSON.stringify($scope.login);

        $('.ajaxLoader').fadeIn("fast");
        //alert(user);
        services.post('login', 'alta_users', user)
                .then(function (data) {

                    if (data.data.success) {
                        console.log(data.data);
                        $('.ajaxLoader').fadeOut("fast");
                        //$scope.login.msg = data.data.message
                        authService.SetCredentials(data.data.users);
                        swal('Good job!', 'El usuario se ha actualizado con éxito!', 'success')
                        $timeout(function () {
                            $location.path('/');
                        }, 6000);

                    } else {

                        $scope.AlertMessage = true;
                        $scope.login.fecha = data.data.error.fecha_nac;
                        $scope.login.msg = data.data.error.message;

                        $scope.login.nombre_error = data.data.error.nombre;
                        $scope.login.email_error = data.data.error.email;
                        console.log(data.data.type_error);
                        $timeout(function () {

                            $scope.AlertMessage = false;

                        }, 5000)
                    }

                });
    }

    ////////////////////////login user
    $scope.singIn = function () {

        var SingINuser = JSON.stringify($scope.signin);
        //alert(SingINuser);
        services.post('login', 'signin', SingINuser)
                .then(function (data) {

                    if (data.data.success) {
                        console.log(data.data.redirect);
                        $scope.login.msg = data.data.message


                        console.log(data.data.users);
                        authService.SetCredentials(data.data.users);

                        console.log($cookies.getObject('user'));
                        window.location = data.data.redirect;

                    } else {
                          $scope.AlertMessage = true;
                        console.log(data.data);
 $timeout(function () {

                            $scope.AlertMessage = false;

                        }, 5000)
                    
                        //$scope.login.msg = data.data.error.message;
                        sweetAlert(data.data.error.nombre);
                        $scope.signin.nombre_err = data.data.error.nombre;
                        $scope.signin.password_error = data.data.error.password;

                    }

                });
    }



    /////////////////////////////empty mistake signin
    $scope.change_signin = function () {
        $scope.signin.nombre_error_sig = "";
        $scope.signin.password_error = "";
    }

    /* $scope.login = {};
     $scope.signup = {};
     
     $scope.doLogin = function (customer) {
     services.post('login', 'signin', customer)
     .then(function (results) {
     console.log(results.data);
     alert(results.data.message);
     
     if (results.data.status == "success") {
     $location.path('dashboard');
     }
     });
     };
     
     $scope.signUp = function (customer) {
     services.post('login', 'signUp', customer)
     .then(function (results) {
     console.log(results.data);
     alert(results.data.message);
     
     if (results.data.status == "success") {
     $location.path('dashboard');
     }
     });
     };
     
     $scope.logout = function () {
     services.get('login','logout')
     .then(function (results) {
     console.log(results.data);
     alert(results.data.message);
     
     $location.path('login');
     });
     }*/
    /*////////////////////////////////////RECOVERY_DESDE_EMAIL*/
    $scope.recovery = function () {
        //console.log("estamos dentro de la funcion de recovery en angular linea 163");


        var result = true;
        var emailreg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
        //console.log("estamos dentro de la funcion de recovery en angular linea 167");

        if ($scope.recovery_email.recov_email === "" || !emailreg.test($scope.recovery_email.recov_email) || $scope.recovery_email.recov_email === "Introduzca su email o el email es incorrecto") {
            /*$("#sed_email_recovery").focus().after("<div class='error'><center>Ingrese un email correcto</center></div>");*/
            $scope.recovery_email.recov_email_error = "Ingrese un email correcto";
            $timeout(function () {
                $(".error").remove();
            }, 3000);

            /*var newDirective = angular.element("<div class='error'><center>Ingrese un email correcto</center></div>");*/
            /*$scope.recovery_email.recov_email.next().append(newDirective);*//*peta*/
            /*$scope.recovery_email.recov_email.append(newDirective);*/
            //console.log("estamos dentro de la funcion de recovery en angular linea 172");
            /*$("#sed_email_recovery").focus().after("<div class='error'><center>Ingrese un email correcto</center></div>");*/
            /*$scope.recovery_email.recov_email.next().insertBefore("<div class='error'><center>Ingrese un email correcto</center></div>");*/
            /*crer elemento dinamicamete por angular*/
            /*var newDirective = angular.element('<div d2></div>');
             element.append(newDirective);
             $compile(newDirective)($scope);*/
            result = false;
        }
        else {
            //console.log("estamos dentro de la funcion de recovery en angular linea 182");
            result = true;
        }

        var email = $scope.recovery_email.recov_email;

        if (result) {
            var data = {
                "email": email
            }

            /*var recovery_email_JSON = JSON.stringify(data);*/
            /*console.log("estamos dentro de la funcion de recovery en angular linea 194 "+recovery_email_JSON);*/
            //console.log("estamos dentro de la funcion de recovery en angular linea 194 ");
            services.post('login', 'email_recovery', data).then(function (data) {

                if (data.data.success) {
                    /*//////////////////fail*/
                    //console.log(data);
                    //console.log(data.data.mensaje);
                    swal(data.data.mensaje);
                    //console.log("estamos dentro de la funcion de recovery en angular linea 202");
                    $timeout(function () {
                        $location.path('/');
                    }, 3000);
                    /*$("#sed_email_recovery").focus().after("<div class='error'>" + data.mensaje + "</div>");*/

                } else {
                    /*//////////////////ok*/
                    //console.log("estamos dentro de la funcion de recovery en angular linea 207")
                    $scope.recovery_email.recov_email_error = data.data.mensaje;
                    /*swal(data.data.mensaje);*/

                    /*$timeout(function () {
                     $location.path('/recovery_pass');
                     }, 3000);/*a utilizar esta*/
                    //console.log("estamos dentro de la funcion de recovery en angular linea 210")

                }

            });

        }

    }

    /*/////////////////////////////////////////////////RECOVERY_PASS*/

    $scope.recovery_pass = function () {
        var result = true;
        var passwordre = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;


        if ($scope.recovery_pass.pass1 == "" || !passwordre.test($scope.recovery_pass.pass1) || $scope.recovery_pass.pass1 == "Introduzca su password") {
            $scope.recovery_pass.recov_pass_error = "Password debe tener al menos 6 caracteres,una letra mayúscula, un carácter numerico";
            /*$("#recovery_pass").focus().after("<span class='error'>Password debe tener al menos 6 caracteres,una letra mayúscula, un carácter numerico</span>");*/
            $timeout(function () {
                $scope.recovery_pass.recov_pass_error = "";
            }, 3000);
            result = false;
        } else if ($scope.recovery_pass.pass2 == "" || !passwordre.test($scope.recovery_pass.pass2) || $scope.recovery_pass.pass2 == "Introduzca su password" || $scope.recovery_pass.pass1 !== $scope.recovery_pass.pass2) {
            $scope.recovery_pass.recov_pass_error1 = "Los 2 password deben coincidir";
            /*$("#recovery_pass2").focus().after("<span class='error'>Los 2 password deben coincidir</span>");*/
            $timeout(function () {
                $scope.recovery_pass.recov_pass_error = "";
            }, 3000);
            result = false;
        } else {

            result = true;
        }

        var password = $scope.recovery_pass.pass1;
        var password1 = $scope.recovery_pass.pass2;
        //console.log(password);
        //console.log(password1);



        if (result) {

            var data = {
                "password": password,
                "password2": password1,
            }

            services.post('login', 'email_recovery_pass', data).then(function (data) {

                if (!data.data.success) {
                    //console.log(data.data);

                    //console.log(data.data.mensaje);  
                    $scope.recovery_pass.recov_pass_error1 = data.data.mensaje;
                    /*swal(data.data.mensaje);*/
                    /*$timeout(function () {
                     $location.path('/login');//Ponemos la ruta sin almohadillas
                     }, 3000);*/

                } else {
                    swal(data.data.mensaje);
                    $timeout(function () {
                        $location.path('/');
                    }, 3000);

                }

            });




        }

    }
/////////////////////////////twitter



    twitterService.initialize();


    //when the user clicks the connect twitter button, the popup authorization window opens
    $scope.connectButton = function () {
        // alert("hola")
        twitterService.connectTwitter().then(function () {
            if (twitterService.isReady()) {
                twitterService.getUserTwitter().then(function (data) {
                    console.log(data);
                    services.post('login', 'social_twitter', data)
                            .then(function (data) {
                                console.log(data)
                                if (data.data.success) {
                                    console.log(data.data.users)

                                    console.log(data.data.users);
                                    authService.SetCredentials(data.data.users);

                                    $cookies.put('avatar', data.data.avatar);
                                    console.log($cookies.getObject('user'));
                                    window.location = data.data.redirect;
                                } else {

                                    swal(data.error);

                                }

                            });
                });
            }
        });
    }





});



