app.controller('ContactCtrl', function ($scope, services,$cookies,FlashService,$timeout, $location) {
  console.log("Estamos dentro del controler contact. modificado");
    if($cookies.getObject('user')){
    
    
    var user_parse=Base64.decode($cookies.getObject('user'));
 //console.log(user);
       var user=JSON.parse(user_parse); 
    }
    
    
    if (user) {
        FlashService.load_header(user);
        FlashService.load_menu(user);
    } else {
        FlashService.load_header("");
        FlashService.load_menu("");
    }

    $scope.contact = {
        name: "",
        email: "",
        message: "",
        mensaje: "partida"
    };
    
    $scope.contact_error = function() {
        $scope.contact.error_contact = "";
        $scope.contact.error_name = "";
        $scope.contact.error_mail = "";
        $scope.contact.error_message = "";
    }
  
    $scope.mensaje = {
       singleSelect: null,
       multipleSelect: [],
       option1: 'partida'
      };
                        
  
    $scope.send = function(){
        
        var name = $scope.contact.name;
        var email = $scope.contact.email;
        var message = $scope.contact.message;
        var mensaje = $scope.contact.mensaje;
        var result = true;
        
        var test_name = /^[a-z ,.'-]+$/i;
        var test_mail = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
        
        if (name == "" || !test_name.test(name)) {
            $scope.contact.error_name = "Introduzca un nombre";
            $timeout(function () {
                $scope.contact.error_name = "";
            }, 5000);
            
            result = false;
            return;

        }
        
        if (email == "" || !test_mail.test(email)) {
            $scope.contact.error_mail = "Introduzca un email correcto";
            $timeout(function () {
                $scope.contact.error_mail = "";
            }, 5000);
            
            result = false;
            return;

        }
        
        if (message == "") {
            $scope.contact.error_message = "Introduzca un mensaje";
            $timeout(function () {
                $scope.contact.error_message = "";
            }, 5000);
            
            result = false;
            return;

        }
        
        
        if (result) {
            
            var data = {"name": name, "email": email, "message":message, "mensaje":mensaje};
            
            var data_contact_JSON = JSON.stringify(data);
            
            //console.log(contact);

            services.post('contact', 'check_contact', data_contact_JSON).then(function (data) {
                if (!data.data.succes) {

                    $scope.contact.error_contact = data.data.mensaje;
                    $timeout(function () {
                        $location.path('/');
                    }, 3000);


                } else {

                    $scope.contact.error_contact = data.data.mensaje;
                    $timeout(function () {
                        $scope.contact.name = "";
                        $scope.contact.email = "";
                        $scope.contact.message = "";
                    }, 3000);

                }

            });
        }
            
            
           
             
        
    }
  
    $scope.mapcontact = function () {
        var myLatlng = new google.maps.LatLng(38.822649, -0.608808);

        var mapOptions = {
            zoom: 14,
            center: myLatlng
        }

        /*lo que utilizabamos en maps*/

        var map = new google.maps.Map(document.getElementById('map'), mapOptions);

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Ubicaion NFU',
            icon: './frontend/assets/img/iconlog.png'
        });
        var contentString = '<div id="bodyContent">' + '<p class="infowindows" >Soterrani de les idees</p></br><a class="infowindows" target="_blank" href="http://maps.google.com/?q=38.822649,-0.608808">' +
                'Click para ver como llegar</a>' + '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });
    }
 
  
});

