
app.filter('startFrom', function () {
    return function (input, start) {
        if (input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});

app.controller('InstallCtrl', function ($scope, services, $timeout, $cookies, FlashService) {
    console.log("Estoy en installation");
    $scope.messageFailure = "";
    $scope.customers = {};

    services.get('installation', 'list_installation')
            .then(function (data) {
                console.log(data);
                if (data.data.success) {
                    
                   // console.log(data.data.nom_install[0]);
                    //$scope.customers = data.data;
                      
                    $scope.list = data.data.nom_install;

                    $scope.currentPage = 1; //current page
                    $scope.entryLimit = 3; //max no of items to display in a page
                    $scope.filteredItems = $scope.list.length; //Initially for no filter  
                    $scope.totalItems = $scope.list.length;
                   ////////////
                } else {
                     $scope.messageFailure = data.data.type_error; 
                }
            });

    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function () {
        $timeout(function () {
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    /*$scope.sort_by = function (predicate) {
     $scope.predicate = predicate;
     $scope.reverse = !$scope.reverse;
     };*/
    
    /////////////////////////menu and header depend user
  if($cookies.getObject('user')){
    
    
    var user_parse=Base64.decode($cookies.getObject('user'));
 //console.log(user);
       var user=JSON.parse(user_parse); 
    }
    
    
    if (user) {
        FlashService.load_header(user);
        FlashService.load_menu(user);
    } else {
        FlashService.load_header("");
        FlashService.load_menu("");
    }

   
});

app.controller('DetailsInstallCtrl', function ($scope, $routeParams, services, install) {
    var installID = ($routeParams.installID) ? parseInt($routeParams.installID) : 0;
    console.log(installID);
   
    
    $scope.install = install.data.install;
    if(install.data.type_error==="503"){
   alert("Not found installation!!");
    }
   
});
