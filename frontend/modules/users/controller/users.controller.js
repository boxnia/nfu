///////////////////////////////perfil_user

app.controller('UsersCtrl', function ($location, $rootScope, FlashService, $timeout, $cookies, $scope, $http, services, authService) {

    ///////////////////////////////acces page  
    if (!$cookies.getObject('user')) {
        $location.path('/')
    }

    console.log("Estoy en users");
    ///////////////////////////////pattern  
    $scope.myRegex_name = /^[a-zA-Z0-9](_(?!(\.|_))|\.(?!(_|\.))|[a-zA-Z0-9]){0,18}[a-zA-Z0-9]$/;
    $scope.myRegex_password = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    $scope.myRegex_email = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    $scope.myRegex_tel = /^(6|7)[0-9]{8}$/;
    //console.log($cookies.getObject('user'));
    ///////////////////////$cookies 
    var user_cookie = Base64.decode($cookies.getObject('user'));
    var user = JSON.parse(user_cookie);
    console.log(user);

///////////////////niveles radio y checkbox del form
    $scope.selectedValue = {
        id: 0
    };
    $scope.selectedValue_nivel = {
        id: 0
    };

    $scope.selectedValue_deportes = {
        id: 0
    };
//////////////////recibir deporte valores asignar true o false
    var todos;
    var futbol;

    var baloncesto;

    var voleibol;
    var tenis;
    var padel;
    if (user.todos === "1") {
        todos = true;
    }
    if (user.futbol === "1") {
        futbol = true;

    }
    if (user.baloncesto === 1) {

        baloncesto = true;
    }
    if (user.voleibol === "1") {
        voleibol = true;
    }
    if (user.tenis === "1") {
        tenis = true;
    }
    if (user.padel === "1") {
        padel = true;
    }

//////////////////////////////////llenar datos form
    $scope.profile = {
        nombre: user.nombre,
        password_old: "",
        password: "",
        password2: "",
        fecha_nac: user.fecha_nac,
        masculino: user.apellidos,
        femenino: user.apellidos,
        radii: [{
                id: 1,
                checked: user.masculino,
                name: "masculino"
            }, {
                id: 2,
                checked: user.femenino,
                name: "femenino"
            }],
        nivel: [{
                id: 1,
                checked: user.bajo,
                name: "bajo"
            }, {
                id: 2,
                checked: user.medio,
                name: "medio"
            }, {
                id: 3,
                checked: user.alto,
                name: "alto"
            }],
        deportes: [
            {
                id: 1,
                checked: todos,
                name: "todos"
            },
            {
                id: 2,
                checked: futbol,
                name: "futbol"
            },
            {
                id: 3,
                checked: baloncesto,
                name: "basket"
            },
            {
                id: 4,
                checked: voleibol,
                name: "volei"
            },
            {
                id: 5,
                checked: tenis,
                name: "tenis"
            },
            {
                id: 6,
                checked: padel,
                name: "padel"
            }
        ],
        //avatar: $cookies.getObject('user').nombre,

        pais: user.pais,
        provincia: user.provincia,
        ciudad: user.ciudad,
        telefono: user.numero,
        email: user.email,
        fecha_registro: user.fecha_registro
    }

//////////////////////////////activar menu y header depend $cookie
    if (user) {
        FlashService.load_header(user);
        FlashService.load_menu(user);
    } else {
        FlashService.load_header("");
        FlashService.load_menu("");
    }

    $scope.open_password = true;
//$cookies.getObject('user');
    $scope.profile.pais = null;
    $scope.paises = [];


///////////////////////load country
    var data = {
        "load_data_pais": true, }
    services.post('users', 'load_pais_users', data)
            .then(function (data) {

                console.log(data)

                if (data.data === 'error') {
                    sweetAlert("Error carga de paises");
                    //  load_countries_v2("app_model/resources/ListOfCountryNamesByName.json");
                } else {
                    if (data.data.length !== 0) {
                        //oorsprong.org
                        $scope.profile.pais = data.data[194].sName;
                        // console.log(data.data[194].sName);
                    } else {
                        sweetAlert("Error carga de paises");
                    }
                }
            })
////////////////////////////////load province
    var data_provincia = {
        "load_data_provincia": true, }

    services.post('users', 'load_provincias_users', data_provincia)
            .then(function (data) {

                if (data.data === 'error') {
                    sweetAlert("Error carga de las provincias");

                } else {
                    $scope.provincia = data.data.provincias

                }
            })

    ///////////////////////////load city
    $scope.selectCiudad = function () {

        var datos = {nombreProvincia: $scope.profile.provincia};
        services.post('users', 'load_poblaciones_users', datos)
                .then(function (data) {
                    if (data.data === 'error') {
                        sweetAlert("Error carga de poblaciones");

                    } else {
                        $scope.ciudades = data.data.poblaciones


                    }
                })
    }

    ///////////////////////////////dropzone upload_image
    $scope.dropzoneConfig = {
        'options': {// passed into the Dropzone constructor
            'url': 'backend/index.php?module=users&function=upload_users',
            addRemoveLinks: true,
            maxFileSize: 1000,
            dictResponseError: "Ha ocurrido un error en el server",
            acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd',
        },
        'eventHandlers': {
            'sending': function (file, xhr, formData) {
            },
            'success': function (file, response) {
                console.log(response);
                var msg = JSON.parse(response)
                console.log(msg);
                if (msg.datos) {
                    // alert("error");
                    //$scope.AlertMessage = true;
                    //$scope.profile.img_error = msg.error;


                    $("#progress").show();
                    $("#bar").width('100%');
                    $("#percent").html('100%');
                    $('.msg').text('').removeClass('msg_error');
                    $('.msg').text('Success Upload image!!').addClass('msg_ok').animate({'right': '300px'}, 300);


                } else {
                    $(".dz-success-mark").remove();
                    $("div").removeClass(".dz-success-mark");
                    $('.msg').text('Error la imagen debe ser 100x100 px!!').addClass('msg_error').animate({'right': '300px'}, 300);
                }
            },
            'removedfile': function (file, serverFileName) {
                $("#progress").hide();
                $('.msg').text('').removeClass('msg_ok');
                $('.msg').text('').removeClass('msg_error');
                $("#e_avatar").html("");

                var json = JSON.parse(data);
                if (json.res === true) {
                    var element;
                    if ((element = file.previewElement) != null) {
                        element.parentNode.removeChild(file.previewElement);
                        //alert("Imagen eliminada: " + name);
                    } else {
                        false;
                    }
                } else { //json.res == false, elimino la imagen también
                    var element;
                    if ((element = file.previewElement) != null) {
                        element.parentNode.removeChild(file.previewElement);
                    } else {
                        false;
                    }
                }
                console.log(file);
                var name = file.name;
                // alert(name);
                console.log(name);
                //var image = JSON.parse(name)
                if (file.xhr.response) {
                    var data = jQuery.parseJSON(file.xhr.response);
                    var data = {'filename': name}

                    /*var config = {
                     method: "POST",
                     url: "http://51.254.97.198/NFU/app_model/index.php?module=users&function=delete_users",
                     data: {'filename': image.datos}
                     }
                     
                     var response = $http(config);
                     response.success(function (data, status, headers, config) {
                     console.log(data);
                     });
                     */
                    services.post('users', 'delete_users', JSON.stringify({'filename': data.datos}))
                            .then(function (data) {

                                console.log(data);
                                if (data.data.res === 'false') {
                                    sweetAlert("Error eliminar imagen");

                                }
                            })


                }

            }
        }
    };
    masculino = user.masculino;
    femenino = user.femenino;
    bajo = user.bajo;
    medio = user.medio;
    alto = user.alto;
//////////////////////functions radio button
////////////////////////////genero
    $scope.handleRadioClick = function (radius) {
        //alert($scope.selectedValue.id);
        if ($scope.selectedValue.id === "masculino") {
            masculino = "true";
            femenino = "false";
        }
        if ($scope.selectedValue.id === "femenino") {
            femenino = "true";
            masculino = "false";
        }
    };
////////////////////////////nivel
    $scope.handleRadioClick_nivel = function (radius) {
        //alert($scope.selectedValue_nivel.id );
        if ($scope.selectedValue_nivel.id === "bajo") {
            bajo = "true";
            medio = "false";
            alto = "false";
        }
        if ($scope.selectedValue_nivel.id === "medio") {
            bajo = "false";
            medio = "true";
            alto = "false";
        }
        if ($scope.selectedValue_nivel.id === "alto") {
            bajo = "false";
            medio = "false";
            alto = "true";
        }




    };


///////////////////////////////////update_user
    $scope.update = function () {
        if ($scope.profile.img_error) {
        } else {

            var provincia;
            var poblacion;
            if (!$scope.profile.provincia) {

                console.log(user);

                if (!user.provincia) {
                    poblacion = "";
                    provincia = "";
                } else {
                    poblacion = user.poblacion;
                    provincia = user.provincia;
                }
            } else {
                if (!$scope.profile.provincia.nombre) {
                    poblacion = user.poblacion;
                    provincia = user.provincia;

                } else {
                    provincia = $scope.profile.provincia.nombre;
                    poblacion = $scope.profile.ciudad.poblacion;
                }

            }
            if ($scope.profile.email !== user.email) {

                $scope.profile.email;

            } else {
                $scope.profile.email = user.email;

            }
            /* if ($scope.profile.password=== "") {
             
             $scope.profile.password = user.password;
             
             } else {
             $scope.profile.password;
             
             }*/
            var deporte = new Array();
            var j = 0;
            for (var i = 0; i < $scope.profile.deportes.length; i++) {
                if ($scope.profile.deportes[i].checked === true) {
                    deporte[j] = $scope.profile.deportes[i].name;
                    j++;

                }
            }

            var data = {
                "nombre": $scope.profile.nombre,
                "pais": $scope.profile.pais,
                "provincia": provincia,
                "poblacion": poblacion,
                "masculino": masculino,
                "femenino": femenino,
                "telefono": $scope.profile.telefono,
                "email": $scope.profile.email,
                "password_bd": user.password,
                "bajo": bajo,
                "medio": medio,
                "alto": alto,
                "password": $scope.profile.password,
                "password2": $scope.profile.password2,
                "fecha_nac": $scope.profile.fecha_nac,
                "fecha_registro": user.fecha_registro,
                "password_old": $scope.profile.password_old,
                "status": user.status,
                "token": user.token,
                "tipo": user.tipo,
                "deporte": deporte,
                "avatar": $rootScope.avatar,
            }

            var datos = JSON.stringify(data)
            console.log(datos);
            services.put('users', 'update_users', data)
                    .then(function (data) {
                        console.log(data)
                        if (data.data.success) {
                            //$scope.profile.ok = data.data.msje;
                            authService.ClearCredentials('user');
//console.log(data.data.users);
                            authService.SetCredentials(data.data.users);


                            //alert(data.data.msje);
                            swal('Good job!', 'El usuario se ha actualizado con éxito!', 'success')

                        } else {
                            $scope.AlertMessage = true;
                            $scope.profile.fecha = data.data.error.fecha_nac;
                            $scope.profile.avatar = data.data.error_avatar;
                            $scope.profile.email_error = data.data.error.email;
                            $scope.profile.password_error = data.data.error.password;
                            $scope.profile.password2_error = data.data.error.password2;
                            $scope.profile.password_old_error = data.data.error.password_old;
                            $scope.profile.password_old_telefono = data.data.error.telefono;

                            $timeout(function () {

                                $scope.AlertMessage = false;

                            }, 5000)

                            if (data.data.type_error === "503") {
                                sweetAlert("Opps... Error en la actualización de la bd");
                            }
                        }

                    });

        }

    }

});

///////////////////////////////perfil_user
app.controller('UsersGamesCtrl', function ($location, $http, FlashService, $cookies, $scope, services, Scroll) {

    console.log("Estoy en UsersGamesCtrl");
    ///////////////////////////////////////$cookies
    var user_cookie = Base64.decode($cookies.getObject('user'));
    var user = JSON.parse(user_cookie);
    ///////////////////////////////acces page  
    if (!$cookies.getObject('user')) {
        $location.path('/')
    }
    ////////////////////////////load menu and header
    if (user) {
        FlashService.load_header(user);
        FlashService.load_menu(user);
    } else {
        FlashService.load_header("");
        FlashService.load_menu("");
    }

    $scope.lat = "0";
    $scope.lng = "0";
    $scope.accuracy = "0";
    $scope.error = "";
    $scope.model = {myMap: undefined};
    $scope.myMarkers = [];
    ////////////////////////scroll infitite with other pluguin angular
    $scope.lat = $cookies.get('latitud');
    $scope.long = $cookies.get('longitud');
    $scope.items = [];
    $scope.busy = false;
    $scope.after = 1;
    $scope.flecha = true;
    $scope.loadmore = "Loading More data..";
    $scope.testData = [];


    $scope.listData = function () {
//alert("hola")
        if ($scope.busy)
            return;
        $scope.busy = true;
        var config = {
            method: "POST",
            url: "backend/index.php?module=users&function=load_coordinates_near",
            data: {"lat_position": $scope.lat,
                "long_position": $scope.long,
                "p": $scope.after, }
        }

        var response = $http(config);
        //console.log(config);
        response.success(function (data) {

            var items = data.game;

            if (!items) {
                $scope.busy = false;
                $scope.flecha = false;
            } else {

                for (var i = 0; i < items.length; i++) {

                    var first_point = new google.maps.LatLng($scope.lat, $scope.long);
                    var second_point = new google.maps.LatLng(items[i].latitud, items[i].longitud);
                    var distancia = google.maps.geometry.spherical.computeDistanceBetween(first_point, second_point);
                    console.log(distancia)
                    if (distancia < 5000) {
                        console.log(items);
                        items[i].i = i + 1;
                        console.log(items[i])

                        $scope.items.push(items[i]);
                        console.log($scope.items);
                        //console.log($scope.items);
                        $scope.market_mov = function (id) {

                            $scope.myMarkers[id].setAnimation(google.maps.Animation.BOUNCE);
                        }


                    }

                }
                $scope.after = $scope.after + 1;
                $scope.busy = false;
            }
        }.bind($scope));

    };


    $scope.listData();







///////////////////////////geolocation








    $scope.showResult = function () {
        return $scope.error == "";
    }

    $scope.mapOptions = {
        center: new google.maps.LatLng($scope.lat, $scope.lng),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    $scope.showPosition = function (position) {
        $scope.lat = position.coords.latitude;
        $scope.lng = position.coords.longitude;
        $scope.accuracy = position.coords.accuracy;
        $scope.$apply();

        var latlng = new google.maps.LatLng($scope.lat, $scope.lng);
        $scope.model.myMap.setCenter(latlng);
        $scope.myMarkers.push(new google.maps.Marker({map: $scope.model.myMap, position: latlng}));

        var data = {
            "lat_position": $scope.lat,
            "long_position": $scope.lng,
        }

        var data_coord = JSON.stringify(data);

        services.post('users', 'load_coordinates_near_map', data_coord)
                .then(function (data) {
                    //console.log(data)
                    if (data.data.success) {

                        //alert(data.data.msje);
                        var game = data.data.game;
                        //console.log(game);
                        for (j = 0; j < game.length; j++) {
                            var first_point = new google.maps.LatLng($scope.lat, $scope.lng);
                            var second_point = new google.maps.LatLng(game[j].latitud, game[j].longitud);
                            var distancia = google.maps.geometry.spherical.computeDistanceBetween(first_point, second_point);

                            console.log(distancia);

                            if (distancia < 5000) {

                                //console.log(deporte);



                                lati = game[j].latitud;
                                longi = game[j].longitud;



                                $scope.myMarkers.push(new google.maps.Marker({
                                    map: $scope.model.myMap,
                                    latitud: lati,
                                    id: game[j].id,
                                    longitud: longi,
                                    position: new google.maps.LatLng(lati, longi),
                                    icon: game[j].deporte,
                                    animation: google.maps.Animation.DROP
                                }));








                            }



                        }

                    } else {

                        /*$scope.profile.fecha = data.data.error.fecha_nac;
                         $scope.profile.avatar = data.data.error_avatar;
                         $scope.profile.email_error = data.data.error.email;
                         $scope.profile.password_error = data.data.error.password;
                         $scope.profile.password2_error = data.data.error.password2;
                         $scope.profile.password_old_error = data.data.error.password_old;
                         $scope.profile.password_old_telefono = data.data.error.telefono;*/
                        if (data.data.type_error === "503") {
                            sweetAlert("Opps... Error en la actualización de la bd");
                        }
                    }



                });
        //alert(data_coord);



        // console.log(response)

        // console.log(data);
        //console.log(response.succes);


    }
    // $timeout(function () {



    // }, 7000)



    $scope.showError = function (error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                $scope.error = "User denied the request for Geolocation."
                break;
            case error.POSITION_UNAVAILABLE:
                $scope.error = "Location information is unavailable."
                break;
            case error.TIMEOUT:
                $scope.error = "The request to get user location timed out."
                break;
            case error.UNKNOWN_ERROR:
                $scope.error = "An unknown error occurred."
                break;
        }
        $scope.$apply();
    }


    $scope.getLocation = function () {

        if (navigator.geolocation) {


            navigator.geolocation.getCurrentPosition($scope.showPosition, $scope.showError);


        }
        else {
            $scope.error = "Geolocation is not supported by this browser.";

        }

    }
    $scope.getLocation();






////////////////////////////////load province
    var data_provincia = {
        "load_data_provincia": true, }

    services.post('users', 'load_provincias_users', data_provincia)
            .then(function (data) {

                if (data.data === 'error') {
                    sweetAlert("Error carga de las provincias");

                } else {


                    $scope.provincia = data.data.provincias

                }
            })



});
function ruta_image(image) {
    //alert(image);
    var project = "./backend/media/"
    var ruta = project + image;
    return ruta;
}



app.filter('startFrom', function () {
    return function (input, start) {
        if (input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});
///////////////////////////////////page admin initial
app.controller('AdminCtrl', function ($cookies, FlashService, $location) {

/////////////////////////////////////////////////////$cookies
    var user_cookie = Base64.decode($cookies.getObject('user'));
    var user = JSON.parse(user_cookie);

    /////////////////////////////////load page if admin
    if (!$cookies.getObject('user')) {
        $location.path('/')
    }

    if (user.tipo !== "admin") {
        $location.path('/');
    }
    ////////////////////////////load menu and header

    if (user) {
        FlashService.load_header(user);
        FlashService.load_menu(user);
    } else {
        FlashService.load_header("");
        FlashService.load_menu("");
    }


});


/////////////////////////////////////////admin list user, create and delete

app.controller('listCtrl', function ($cookies, $scope, FlashService, services, $timeout, $filter, $location, $window) {

    ////////////////////////////////////////////////$cookies
    var user_cookie = Base64.decode($cookies.getObject('user'));
    var user = JSON.parse(user_cookie);
    //console.log(user);
    /////////////////////////////////load page if admin
    if (!$cookies.getObject('user')) {
        $location.path('/')
    }

    if (user.tipo !== "admin") {
        $location.path('/')
    }
    ////////////////////////////load menu and header

    if (user) {
        FlashService.load_header(user);
        FlashService.load_menu(user);
    } else {
        FlashService.load_header("");
        FlashService.load_menu("");
    }


    $scope.messageFailure = "";
    $scope.users = {};
//////////////////////////////list users admin
    services.post('users', 'obtain_users')

            .then(function (data) {
                console.log(data)
                if (data.status == 204) {
                    $scope.messageFailure = "Not found users!!"; ////////////
                } else {
                    //$scope.customers = data.data;
                    $scope.list = data.data.users;
                    $scope.currentPage = 1; //current page
                    $scope.entryLimit = 5; //max no of items to display in a page
                    $scope.filteredItems = $scope.list.length; //Initially for no filter  
                    $scope.totalItems = $scope.list.length;
                }
            });
    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };
    //////////////////filter list
    $scope.filter = function () {
        $timeout(function () {
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    ////////////////////order list
    $scope.sort_by = function (predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };

    $scope.login = {
        nombre: "",
        password: "",
        password2: "",
        email: "",
        fecha_nac: "",
        fecha_registro: $filter('date')(new Date(), 'yyyy-MM-dd'),
        fecha: "",
        radii: [{
                id: 1,
                checked: true,
                name: "admin"
            }, {
                id: 2,
                checked: false,
                name: "normal"
            }],
    }


    var valor;
    valor = "admin";
    //////////////////////////////functions radio button
    //////////////////////tipo admin
    $scope.handleRadioClick = function (radius) {

        //alert($scope.selectedValue.id);
        if ($scope.selectedValue.id === "normal") {
            valor = "normal";

        }
        if ($scope.selectedValue.id === "admin") {
            valor = "admin";

        }
    };


    $scope.selectedValue = {
        id: 0
    };
///////////////////////////////alta user admin
    $scope.altaUser = function () {
        var data = {
            nombre: $scope.login.nombre,
            password: $scope.login.password,
            email: $scope.login.email,
            fecha_nac: $scope.login.fecha_nac,
            fecha_registro: $filter('date')(new Date(), 'yyyy-MM-dd'),
            tipo: valor,
        }
        console.log(data);
        var user = JSON.stringify(data);
        //alert(user);
        services.post('login', 'alta_users', user)

                .then(function (data) {

                    if (data.data.success) {
                        console.log(data.data);
                        $scope.login.msg = ""
                        swal("El usuario ha sido creado con éxito")
                        //authService.SetCredentials(data.data.users);

                        $timeout(function () {
                            $location.path('/admin');
                        }, 6000);

                    } else {

                        $scope.login.fecha = data.data.error.fecha_nac;
                        $scope.login.msg = data.data.error.message;
                        $scope.login.nombre_error = data.data.error.nombre;
                        $scope.login.email_error = data.data.error.email;
                        console.log(data.data.type_error);
                    }

                });
    }
///////////////////////////////delete user admin
    $scope.deleteUser = function (nombre) {
        console.log(nombre)
        //$location.path('/');
        swal({title: 'Are you sure?',
            text: 'You will not be able to recover this imaginary file!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel plx!',
            confirmButtonClass: 'confirm-class',
            cancelButtonClass: 'cancel-class', closeOnConfirm: false, closeOnCancel: false},
        function (isConfirm) {
            if (isConfirm) {
                services.delete('users', 'delete_user', nombre)
                        .then(function (data) {
                            console.log(data);
                            if (data.data.success === true) {
                                console.log(data.data.msje);
                                swal('Deleted!', 'Your file has been deleted.', 'success');

                                $timeout(function () {

                                    $window.location.reload();
                                }, 3000)

                                console.log($location.path());
                            } else {
                                sweetAlert(data.data.error);

                            }

                        });


            } else {
                swal('Cancelled', 'Your imaginary file is safe :)', 'error');
            }
        });

    };
});


/////////////////////////////////////edit user admin
app.controller('EditCtrl', function ($cookies, user_bd, authService, $location, FlashService, $timeout, $scope, services) {
    console.log("Estoy en edit users");
///////////////////////////////////////$cookies
    var user_cookie = Base64.decode($cookies.getObject('user'));
    var user = JSON.parse(user_cookie);
    //console.log(user);
    /////////////////////////////////load page if admin
    if (!$cookies.getObject('user')) {
        $location.path('/')
    }

    if (user.tipo !== "admin") {
        $location.path('/')
    }
    ////////////////////////////load menu and header

    if (user) {
        FlashService.load_header(user);
        FlashService.load_menu(user);
    } else {
        FlashService.load_header("");
        FlashService.load_menu("");
    }
//////////////////////////////pattern
    $scope.myRegex_name = /^[a-zA-Z0-9](_(?!(\.|_))|\.(?!(_|\.))|[a-zA-Z0-9]){0,18}[a-zA-Z0-9]$/;
    $scope.myRegex_password = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    $scope.myRegex_email = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    $scope.myRegex_tel = /^(6|7)[0-9]{8}$/;
    //console.log($cookies.getObject('user'));
///////////////////////////user_bd load user with token
    user = user_bd.data.users;
    console.log(user);
///////////////////niveles radio y checkbox del form
    $scope.selectedValue = {
        id: 0
    };
    $scope.selectedValue_nivel = {
        id: 0
    };

    $scope.selectedValue_deportes = {
        id: 0
    };

    var todos;
    var futbol;
    var baloncesto;
    var voleibol;
    var tenis;
    var padel;
    ////////////////////////////Assign value sports
    if (user.todos === "1") {
        todos = true;
    }
    if (user.futbol === "1") {
        futbol = true;
    }

    if (user.baloncesto === 1) {
        baloncesto = true;
    }

    if (user.voleibol === "1") {
        voleibol = true;
    }
    if (user.tenis === "1") {
        tenis = true;
    }
    if (user.padel === "1") {

        padel = true;
    }

//////////////////////////////////fill in data form
    $scope.profile = {
        nombre: user.nombre,
        password_old: "",
        password: "",
        password2: "",
        fecha_nac: user.fecha_nac,
        masculino: user.apellidos,
        femenino: user.apellidos,
        radii: [{
                id: 1,
                checked: user.masculino,
                name: "masculino"
            }, {
                id: 2,
                checked: user.femenino,
                name: "femenino"
            }],
        nivel: [{
                id: 1,
                checked: user.bajo,
                name: "bajo"
            }, {
                id: 2,
                checked: user.medio,
                name: "medio"
            }, {
                id: 3,
                checked: user.alto,
                name: "alto"
            }],
        deportes: [
            {
                id: 1,
                checked: todos,
                name: "todos"
            },
            {
                id: 2,
                checked: futbol,
                name: "futbol"
            },
            {
                id: 3,
                checked: baloncesto,
                name: "basket"
            },
            {
                id: 4,
                checked: voleibol,
                name: "volei"
            },
            {
                id: 5,
                checked: tenis,
                name: "tenis"
            },
            {
                id: 6,
                checked: padel,
                name: "padel"
            }
        ],
        //avatar: $cookies.getObject('user').nombre,

        pais: user.pais,
        provincia: user.provincia,
        ciudad: user.ciudad,
        telefono: user.numero,
        email: user.email,
        fecha_registro: user.fecha_registro
    }

    $scope.open_password = true;
//$cookies.getObject('user');
    $scope.profile.pais = null;
    $scope.paises = [];

///////////////////////load country
    var data = {
        "load_data_pais": true, }
    services.post('users', 'load_pais_users', data)
            .then(function (data) {

                if (data.data === 'error') {
                    sweetAlert("Error carga de paises");
                } else {
                    $scope.profile.pais = data.data[194].sName;

                }
            })
////////////////////////////////load province
    var data_provincia = {
        "load_data_provincia": true, }

    services.post('users', 'load_provincias_users', data_provincia)
            .then(function (data) {

                //console.log(data.data)

                if (data.data === 'error') {
                    sweetAlert("Error carga de las provincias");

                } else {
                    $scope.provincia = data.data.provincias

                }
            })

    ///////////////////////////load city
    $scope.selectCiudad = function () {
        // console.log($scope.profile.provincia);

        var datos = {nombreProvincia: $scope.profile.provincia};

        services.post('users', 'load_poblaciones_users', datos)
                .then(function (data) {

                    if (data.data === 'error') {
                        sweetAlert("Error carga de poblaciones");

                    } else {
                        $scope.ciudades = data.data.poblaciones
                    }
                })
    }
     ///////////////////////////////dropzone upload_image
    $scope.dropzoneConfig = {
        'options': {// passed into the Dropzone constructor
            'url': 'backend/index.php?module=users&function=upload_users',
            addRemoveLinks: true,
            maxFileSize: 1000,
            dictResponseError: "Ha ocurrido un error en el server",
            acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd',
        },
        'eventHandlers': {
            'sending': function (file, xhr, formData) {
            },
            'success': function (file, response) {
                console.log(response);
                var msg = JSON.parse(response)
                console.log(msg);
                if (msg.datos) {
                    // alert("error");
                    //$scope.AlertMessage = true;
                    //$scope.profile.img_error = msg.error;


                    $("#progress").show();
                    $("#bar").width('100%');
                    $("#percent").html('100%');
                    $('.msg').text('').removeClass('msg_error');
                    $('.msg').text('Success Upload image!!').addClass('msg_ok').animate({'right': '300px'}, 300);


                } else {
                    $(".dz-success-mark").remove();
                    $("div").removeClass(".dz-success-mark");
                    $('.msg').text('Error la imagen debe ser 100x100 px!!').addClass('msg_error').animate({'right': '300px'}, 300);
                }
            },
            'removedfile': function (file, serverFileName) {
                $("#progress").hide();
                $('.msg').text('').removeClass('msg_ok');
                $('.msg').text('').removeClass('msg_error');
                $("#e_avatar").html("");

                var json = JSON.parse(data);
                if (json.res === true) {
                    var element;
                    if ((element = file.previewElement) != null) {
                        element.parentNode.removeChild(file.previewElement);
                        //alert("Imagen eliminada: " + name);
                    } else {
                        false;
                    }
                } else { //json.res == false, elimino la imagen también
                    var element;
                    if ((element = file.previewElement) != null) {
                        element.parentNode.removeChild(file.previewElement);
                    } else {
                        false;
                    }
                }
                console.log(file);
                var name = file.name;
                // alert(name);
                console.log(name);
                //var image = JSON.parse(name)
                if (file.xhr.response) {
                    var data = jQuery.parseJSON(file.xhr.response);
                    var data = {'filename': name}

                    /*var config = {
                     method: "POST",
                     url: "http://51.254.97.198/NFU/app_model/index.php?module=users&function=delete_users",
                     data: {'filename': image.datos}
                     }
                     
                     var response = $http(config);
                     response.success(function (data, status, headers, config) {
                     console.log(data);
                     });
                     */
                    services.post('users', 'delete_users', JSON.stringify({'filename': data.datos}))
                            .then(function (data) {

                                console.log(data);
                                if (data.data.res === 'false') {
                                    sweetAlert("Error eliminar imagen");

                                }
                            })


                }

            }
        }
    };
    masculino = user.masculino;
    femenino = user.femenino;
    bajo = user.bajo;
    medio = user.medio;
    alto = user.alto;
/////////////////////////////////////////////functions change radio button
//////////////////////////////////genero
    $scope.handleRadioClick = function (radius) {
        //alert($scope.selectedValue.id);
        if ($scope.selectedValue.id === "masculino") {
            masculino = "true";
            femenino = "false";
        }
        if ($scope.selectedValue.id === "femenino") {
            femenino = "true";
            masculino = "false";
        }
    };
//////////////////////////////////nivel
    $scope.handleRadioClick_nivel = function (radius) {
        //alert($scope.selectedValue_nivel.id );
        if ($scope.selectedValue_nivel.id === "bajo") {
            bajo = "true";
            medio = "false";
            alto = "false";
        }
        if ($scope.selectedValue_nivel.id === "medio") {
            bajo = "false";
            medio = "true";
            alto = "false";
        }
        if ($scope.selectedValue_nivel.id === "alto") {
            bajo = "false";
            medio = "false";
            alto = "true";
        }

    };
////////////////////////////////////////////////update user admin
    $scope.update = function () {
        if ($scope.profile.img_error) {
        } else {
            var provincia;
            var poblacion;
            if (!$scope.profile.provincia) {

                console.log(user);

                if (!user.provincia) {
                    poblacion = "";
                    provincia = "";
                } else {
                    poblacion = user.poblacion;
                    provincia = user.provincia;
                }
            } else {
                if (!$scope.profile.provincia.nombre) {
                    poblacion = user.poblacion;
                    provincia = user.provincia;

                } else {
                    provincia = $scope.profile.provincia.nombre;

                    poblacion = $scope.profile.ciudad.poblacion;
                }

            }

            if ($scope.profile.email !== user.email) {

                $scope.profile.email;

            } else {
                $scope.profile.email = user.email;

            }


            var deporte = new Array();
            var j = 0;
            for (var i = 0; i < $scope.profile.deportes.length; i++) {
                if ($scope.profile.deportes[i].checked === true) {
                    deporte[j] = $scope.profile.deportes[i].name;
                    j++;

                }
            }

            var data = {
                "nombre": $scope.profile.nombre,
                "pais": $scope.profile.pais,
                "provincia": provincia,
                "poblacion": poblacion,
                "masculino": masculino,
                "femenino": femenino,
                "telefono": $scope.profile.telefono,
                "email": $scope.profile.email,
                "password_bd": user.password,
                "bajo": bajo,
                "medio": medio,
                "alto": alto,
                "password2": $scope.profile.password2,
                "fecha_nac": $scope.profile.fecha_nac,
                "fecha_registro": $scope.profile.fecha_registro,
                "password_old": $scope.profile.password_old,
                "status": user.status,
                "token": user.token,
                "tipo": user.tipo,
                "deporte": deporte,
                "avatar": user.avatar,
            }
            var datos = JSON.stringify(data)
            console.log(datos);
            services.put('users', 'update_users', data)
                    .then(function (data) {
                        console.log(data)
                        if (data.data.success) {


                            //alert(data.data.msje);
                            //swal(data.data.msje)
                            swal('Good job!', 'El usuario se ha actualizado con éxito!', 'success')
                        } else {
                            $scope.AlertMessage = true;
                            $timeout(function () {

                                $scope.AlertMessage = false;

                            }, 5000)
                            $scope.profile.fecha = data.data.error.fecha_nac;
                            $scope.profile.avatar = data.data.error_avatar;
                            $scope.profile.email_error = data.data.error.email;
                            $scope.profile.password_error = data.data.error.password;
                            $scope.profile.password2_error = data.data.error.password2;
                            $scope.profile.password_old_error = data.data.error.password_old;
                            $scope.profile.password_old_telefono = data.data.error.telefono;
                            if (data.data.type_error === "503") {
                                sweetAlert("Oops... Error en la actualización de la bd");

                            }
                        }
                    });

        }

    }

});

